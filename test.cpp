#include "iostream"
using namespace std;

int main()
{
  int *a;

  a = new int(9);
  cout << *a << endl;

  int& b = *a;
  b++;
  cout << *a << endl;
  delete a;
  return 0;
}
